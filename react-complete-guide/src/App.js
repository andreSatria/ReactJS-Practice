import React, { Component } from 'react';
import './App.css';

import Person from './Person/Person.js';

class App extends Component {
  state = { // only available in Components, (extend components) //Not suggested!

    persons: [
        {name:"Max", age: 28},
        {name:"Manu", age:29},
        {name:"Stephanie", age:26}
    ],

    otherState: "Some other value",
    showPersons:false,
  }

  switchNameHandler = (newName) =>  {//Not suggested to directly change state
    //console.log("Was clicked!");
    // DONT DO THIS! this.state.persons[0].name = "Maximilian";
    this.setState({
      persons:[
        {name:newName, age: 28},
        {name:"Manu", age:29},
        {name:"Stephanie", age:27}
      ]

    } )

  }

  nameChangedHandler = (event) => {
    this.setState({
      persons:[
        {name:"Max", age: 28},
        {name:event.target.value, age:29},
        {name:"Stephanie", age:27}
      ]

    } )
  }

  togglePersonsHandler= () => {
    const doesShow = this.state.showPersons;
    this.setState({showPersons: !doesShow})
  }

  render(){

    const style = {
      backgroundColor:'white',
      font:'inherit',
      border:'1px solid blue',
      padding:"8px",
      cursor:'pointer',
    };



    return (
      <div className="App">
        <h1>Hi, im a react app! LOL  </h1>
        <p>This is really working!</p>
        <button
          style={style}
          onClick={this.togglePersonsHandler}>Toggle Person!</button>
        {
          this.state.showPersons === true ?
          <div>
          <Person
            name={this.state.persons[0].name}
            age={this.state.persons[0].age} />
            <Person
            name={this.state.persons[1].name}
            age={this.state.persons[1].age}
            click={this.switchNameHandler.bind(this, "Max")}
            changed={this.nameChangedHandler}>My Hobby: Racing</Person>
          <Person
            name={this.state.persons[2].name}
            age={this.state.persons[2].age}/>
        </div> : null
      }
      </div>

    );
    //return React.createElement('div',{className : "App"},React.createElement("h1", null, "Does this work now?"))
  }
}

export default App;
