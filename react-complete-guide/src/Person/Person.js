import React from 'react';

import './Person.css'


const person = (props) => { //A function to listen to an event , which does not directly manipulate the App.js
  return(
    <div className="Person">
      <p onClick={props.click}> I'm {props.name} and I am {props.age} years old!</p>
      <p> {props.children}</p>
      <input type="text" onChange={props.changed} value={props.name}/>
    </div>

  )
};


export default person;
